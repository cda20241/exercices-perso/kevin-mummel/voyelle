import unidecode

# Création d'une liste Voyelles
VOYELLES = ['a', 'e', 'i', 'o', 'u', 'y']


# Création de la fonction nombre_voyelles
def nombre_voyelles(p_phrase: str) -> int:
    """
    Fonction qui compte le nombre de voyelle incluant majuscule et accent
    """
    # initialiser le compteur à 0
    Nb_voyelles = 0
    # On créer une boucle
    for lettre in p_phrase:
        # Enlever les accents
        lettre_sans_accent = unidecode.unidecode(lettre)
        # Si le texte en minuscule est dans la liste Voyelles
        if lettre_sans_accent.lower() in VOYELLES:
            # Incrémenter de 1
            Nb_voyelles += 1
    # On renvoie la valeur à la variable Nb_Voyelles
    return Nb_voyelles

def nombre_chaque_voyelle(p_phrase: str) -> dict:
    """
     Fonction qui compte le nombre de chaque voyelle et qui l'incrémente au fur et à mesure
    """
    # initialiser les compteurs à 0
    compte_voyelles = {voyelle: 0 for voyelle in VOYELLES}

    for lettre in p_phrase:
        # Enlever les accents
        lettre_sans_accent = unidecode.unidecode(lettre)
        # Convertir la lettre en minuscules
        lettre_sans_accent = lettre_sans_accent.lower()

        if lettre_sans_accent in VOYELLES:
        # Si la lettre sans accent est présente dans la list VOYELLE
            compte_voyelles[lettre_sans_accent] += 1
            # On incrémente de 1 compte_voyelle

    return compte_voyelles
    # On renvoie la valeur de compte_voyelle en début de boucle

if __name__ == '__main__':
    # On demande à l'utilisateur d'entrer une phrase.
    phrase = input("Entrez une phrase svp : ")
    # On affiche la variable Nb_Voyelles dans la fonction nombre_voyelles(phrase)
    print("Il y a", nombre_voyelles(phrase), "voyelles dans la phrase.")
    print(nombre_chaque_voyelle(phrase))
